CREATE DATABASE lab;
\c lab

DROP TABLE IF EXISTS Player CASCADE;
DROP TABLE IF EXISTS Shop CASCADE;
DROP TABLE IF EXISTS Inventory CASCADE;

CREATE TABLE Player (
    username TEXT UNIQUE, 
    balance  INT CHECK(balance >= 0)
);

CREATE TABLE Shop (
    product  TEXT UNIQUE, 
    in_stock INT CHECK(in_stock >= 0), 
    price    INT CHECK (price >= 0)
);

CREATE TABLE Inventory (
    id       serial PRIMARY KEY NOT NULL,
    username TEXT           NOT NULL REFERENCES Player (username),
    product  TEXT           NOT NULL REFERENCES Shop (product),
    amount   INT CHECK (amount >= 0),
    UNIQUE (username, product)
);






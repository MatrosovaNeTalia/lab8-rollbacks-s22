import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=admin user=nataliamatrosova")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"

# Homework
buy_query_inventory_sum = "SELECT sum(amount) FROM Inventory WHERE username = %(username)s"
buy_update_inventory = "UPDATE Inventory SET amount = amount + %(amount)s WHERE username = %(username)s AND product = %(product)s"



def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            try:
                cur.execute(buy_query_inventory_sum)
                n_items = cur.fetchone()[0]
                if n_items + amount > 100:
                    raise Exception("No more than 100 items in Inventory!")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad Inventory update")
            
            cur.execute(buy_update_inventory, obj)
            conn.commit()

buy_product("Bob", "marshmello", 1)
# buy_product("Bob", "marshmello", 60)
